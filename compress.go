package xtgz

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

/*
	Compress 解压文件
	@Param tgzName tgz文件的名称，比如： tgzPath/test.tgz
	@Param dirName 需要压缩的文件路径，当为 ""或者"." 时为当前文件夹
*/
func Compress(tgzName, dirName string) (tgzErr error) {
	tgzName = strings.ReplaceAll(tgzName, " ", "")
	if tgzName == "" {
		return fmt.Errorf("file name is nil with tgz name :%s", tgzName)
	}
	// 判断 tgzName中是否有路径，如有的话，先创建文件夹路径，然后创建文件
	err := dirExist(filepath.Dir(tgzName), 0644)
	if err != nil {
		return err
	}
	// 遍历指定文件夹中所有文件，获取文件列表
	files, err := allFilesInDir(dirName)
	if err != nil {
		return err
	}

	// Create output file
	out, err := os.Create(tgzName)
	if err != nil {
		return fmt.Errorf("error writing archive :%s", err.Error())
	}
	defer func(out *os.File) {
		err := out.Close()
		if err != nil {
			tgzErr = err
		}
	}(out)

	// Create the archive and write the output to the "out" Writer
	err = createArchive(files, out)
	if err != nil {
		return fmt.Errorf("error creating archive:%s", err.Error())
	}

	return nil
}

func allFilesInDir(dirName string) (fpList []string, err error) {
	if strings.TrimSpace(dirName) == "" {
		dirName = "."
	}
	err = filepath.Walk(dirName,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			//fmt.Println(path, info.Size())
			fpList = append(fpList, path)
			return nil
		})

	return fpList, err
}

func createArchive(files []string, buf io.Writer) (caErr error) {
	// Create new Writers for gzip and tar
	// These writers are chained. Writing to the tar writer will
	// write to the gzip writer which in turn will write to
	// the "buf" writer
	gw := gzip.NewWriter(buf)
	defer func(gw *gzip.Writer) {
		err := gw.Close()
		if err != nil {
			caErr = err
		}
	}(gw)

	tgw := tar.NewWriter(gw)
	defer func(tw *tar.Writer) {
		err := tw.Close()
		if err != nil {
			caErr = err
		}
	}(tgw)

	// Iterate over files and add them to the tar archive
	for _, file := range files {
		err := addToArchive(tgw, file)
		if err != nil {
			return err
		}
	}

	return nil
}

func addToArchive(tgw *tar.Writer, filename string) (ataErr error) {
	// Open the file which will be written into the archive
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			ataErr = err
		}
	}(file)

	// Get FileInfo about our file providing file size, mode, etc.
	info, err := file.Stat()
	if err != nil {
		return err
	}

	// Create a tar Header from the FileInfo data
	header, err := tar.FileInfoHeader(info, info.Name())
	if err != nil {
		return err
	}

	// Use full path as name (FileInfoHeader only takes the basename)
	// If we don't do this the directory strucuture would
	// not be preserved
	// https://golang.org/src/archive/tar/common.go?#L626
	if runtime.GOOS == "windows" {
		// windows filepath "\\" to “/”
		filename = strings.ReplaceAll(filename, "\\", "/")
	}

	header.Name = filename

	// Write file header to the tar archive
	err = tgw.WriteHeader(header)
	if err != nil {
		return err
	}

	// 判断下文件是否是标准文件，如果不是就不处理了，
	// 如： 目录，这里就只记录了文件信息，不会执行下面的 copy
	if !info.Mode().IsRegular() {
		return nil
	}

	// Copy file content to tar archive
	_, err = io.Copy(tgw, file)
	if err != nil {
		return err
	}

	return nil
}
