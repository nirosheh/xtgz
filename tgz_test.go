package xtgz

import (
	"os"
	"strconv"
	"testing"
	"time"
)

func TestClearTarget(t *testing.T) {
	os.RemoveAll("target")
}

// for linux
func TestCompressLinux(t *testing.T) {
	err := Compress("target/tgzName.tgz", "tgzDir")
	if err != nil {
		t.Fatal(err)
	}
}

// for windows
func TestCompressWin(t *testing.T) {
	err := Compress("target\\tgzName.tgz", "tgzDir")
	if err != nil {
		t.Fatal(err)
	}
}

// for linux
func TestDecompressLinux(t *testing.T) {
	err := Decompress("target/tgzName.tgz", "target/"+strconv.FormatInt(time.Now().Unix(), 10))
	if err != nil {
		t.Fatal(err)
	}
}

// fow windows
func TestDecompressWin(t *testing.T) {
	err := Decompress("target\\tgzName.tgz", "target\\"+strconv.FormatInt(time.Now().Unix(), 10))
	if err != nil {
		t.Fatal(err)
	}
}
