# xtgz Project

A Go library to compress|decompress tgz files to specified directories.

# Example

The following program will compress the directory "tgzDir" to a specified file "target/tgzName.tgz", then decompress
"target/tgzName.tgz" to unixtime directory  in "target", and delete the "target" directory:

```go
package main

import (
	"fmt"
	"os"

	"gitee.com/nirosheh/xtgz"
)

func main() {
    // 将示例文件夹压缩成 tgz
    err := xtgz.Compress("target/tgzName.tgz", "tgzDir")
    if err != nil {
        fmt.Println(err)
    }

    // 将上述 tgz 解压
    err = xtgz.Decompress("target/tgzName.tgz", "target/"+strconv.FormatInt(time.Now().Unix(), 10))
    if err != nil {
        fmt.Println(err)
    }

    // 删除 target 文件夹
    os.RemoveAll("target")
}
```
