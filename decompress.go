package xtgz

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
)

/*
	Decompress 解压tgz文件
	@Param tgz string :tgz文件路径，可以是相对路径，也可以是绝对路径。
	@Param des string :tgz文件解压的目标路径，可以是相对路径，也可以是绝对路径。
*/
func Decompress(tgz, dest string) (err error) {
	err = dirExist(dest, 0644)
	if err != nil {
		return
	}
	f, err := os.Open(tgz)
	if err != nil {
		return err
	}

	defer func(f *os.File) {
		errClose := f.Close()
		if err == nil {
			err = errClose
		}
	}(f)

	tarReader, err := gzTarReader(f)
	if err != nil {
		return err
	}

	if err = unTar(tarReader, dest); err != nil {
		return err
	}

	return nil
}

func gzTarReader(r io.Reader) (*tar.Reader, error) {
	gzReader, err := gzip.NewReader(r)
	if err != nil {
		return nil, err
	}

	return tar.NewReader(gzReader), nil
}

func unTar(src *tar.Reader, dstPath string) (err error) {
	for {
		header, err := src.Next()

		switch {
		case err == io.EOF:
			return nil
		case err != nil:
			return err
		case header == nil:
			continue
		}

		dst := filepath.Join(dstPath, header.Name)
		mode := os.FileMode(header.Mode)
		switch header.Typeflag {
		case tar.TypeDir:
			err := dirExist(dst, mode)
			if err != nil {
				return err
			}
		case tar.TypeReg:
			err := makeFile(dst, mode, src)
			if err != nil {
				return err
			}
		default:
			return fmt.Errorf("unable to untar type : %c in file %s", header.Typeflag, header.Name)
		}
	}
}

func makeFile(path string, mode os.FileMode, contents io.Reader) (err error) {
	w, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR, mode)

	//w, err := os.Create(path)
	if err != nil {
		return err
	}
	defer func(w *os.File) {
		errClose := w.Close()
		if err == nil {
			err = errClose
		}
	}(w)

	_, err = io.Copy(w, contents)
	if err != nil {
		return err
	}
	/*if err = os.Chmod(path, mode); err != nil {
		return err
	}*/
	return nil
}

// 判断文件夹是否存在，是否有同名的文件
// 如果不存在则创建文件夹，如果有同名文件报错并推出
// 如果其他错误 报错并退出
func dirExist(dir string, perm os.FileMode) (err error) {
	f, err := os.Open(dir)
	if err != nil {
		// errors.Is(err, fs.ErrExist)
		if errors.Is(err, fs.ErrNotExist) {
			// 如果文件夹不存在 新建文件夹
			err = os.MkdirAll(dir, perm)
			if err != nil {
				return
			}
		}
		return
	}

	defer func(f *os.File) {
		fcErr := f.Close()
		if fcErr != nil {
			err = fcErr
		}
	}(f)

	info, err := f.Stat()
	if err != nil {
		return err
	}
	if !info.IsDir() {
		err = fmt.Errorf("%s is regular file ,not a directory", info.Name())
	}
	return
}
